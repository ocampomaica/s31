// 1. What directive is used by Node.js in loading the modules it needs?
// Answer: require



// 2. What Node.js module contains a method for server creation?
// Answer: http



// 3. What is the method of the http object responsible for creating a server using Node.js?
// Answer: createServer


// 4. What method of the response object allows us to set status codes and content types?
// Answer: end


// 5. Where will console.log() output its contents when run in Node.js?
// Answer:

// 6. What property of the request object contains the address's endpoint?
// Answer: url


// Simple Server

/*
	Instructions:

	- Import the http module using the required directive.
	- Create a variable port and assign it with the value of 3000.
	- Create a server using the createServer method that will listen in to the port provided above.
	- Console log in the terminal a message when the server is successfully running.
	- Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
	- Access the login route to test if it’s working as intended.
	- Create a condition for any other routes that will return an error message.
	- Access any other route to test if it’s working as intended.

*/

//Code here:

const http = require('http')
const port = 3000

const server = http.createServer(function(request,response){

	console.log('Server is successfully running')

	if (request.url == '/') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the page')
	} else if (request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the login page')
	} else {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Error - Page not found')
	}
})

server.listen(port)














