const http = require('http')
const port = 4000

const server = http.createServer(function(request,response){
	if (request.url == '/greeting') {

		// You can use request.url to get the current destination of the user in the browser. You can also check if the current destination of the user matches with the endpoint and if it does, do the respective processes for each end point.
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Hello Batch 197')
	} else if (request.url == '/homepage') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the homepage')
	} else {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Page not available')
	}
})

server.listen(port)
console.log(`Server now accesible at localhost: ${port}`)














